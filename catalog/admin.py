from django.contrib import admin

from catalog.models import Company, Course

# Register your models here.

admin.site.register(Company)
admin.site.register(Course)