from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Company (models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField ()

	def __unicode__(self):
	     return self.name

	class Meta:
		verbose_name_plural = 'companies'

class Course(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
	company = models.ForeignKey(Company)

	def __unicode__(self):
	    return self.name
			
