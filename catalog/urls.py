from django.conf.urls import url

from catalog import views


urlpatterns = [
    url(r'^$', views.companies_list, name='companies_list'),
    url(r'new/$', views.add_company, name='add_company'),
    url(r'^(?P<company_id>[0-9]+)/$', views.company_detail, name='company_detail'),
    
    
]
