from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse


from catalog.models import Company, Course

# Create your views here.
def companies_list(request):
	companies = Company.objects.all()
	context_dict = {'companies_list': companies}
	return render (request, 'catalog/companies_list.html', context_dict)

def company_detail(request, company_id):

		company = Company.objects.get(pk=int(company_id))
		courses = Course.objects.filter(company=company_id)
		context_dict = {'company': company, 'courses': courses}
		return render (request, 'catalog/companies_detail.html', context_dict)
    

    
def add_company(request):
	return HttpResponse('Creating company') 
	